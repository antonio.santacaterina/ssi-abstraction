FROM ubuntu:18.04 as base

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y && apt-get install -y \
    software-properties-common \
    apt-transport-https \
    curl \
    # Only needed to build indy-sdk
    build-essential

# libindy
RUN curl -fsSL 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xCE7709D068DB5E88' | apt-key add -
RUN add-apt-repository "deb https://repo.sovrin.org/sdk/deb bionic stable"

# nodejs
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash

# yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

# install depdencies
RUN apt-get update -y && apt-get install -y --allow-unauthenticated \
    libindy \
    nodejs

# Install yarn seperately due to `no-install-recommends` to skip nodejs install
RUN apt-get install -y --no-install-recommends yarn

WORKDIR /usr/src/app

RUN yarn global add @nestjs/cli

COPY package.json ./
COPY yarn.lock ./

# --prod works if @types/node is in deps (not devDeps)
RUN yarn --frozen-lockfile --prod

COPY . .

RUN yarn build



FROM base as final

# ENV RUN_MODE="docker"

WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH

COPY package.json ./

COPY --from=base /usr/src/app/dist ./dist
COPY --from=base /usr/src/app/start.sh ./start.sh
COPY --from=base /usr/src/app/node_modules ./node_modules

EXPOSE 3009
EXPOSE 3010
EXPOSE 4000

RUN chmod +x ./start.sh

CMD ["./start.sh"]

